<<<<<<< HEAD
package ku.sci.th.myapp;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.util.ArrayList;

import org.junit.Test;


public class TestAppointment {
	
	@Test
	public void testCreateAppointment() throws ParseException {
<<<<<<< HEAD
		Appointment a = new AppOnetime("Baitoey", "4/09/2558 12:00");
		assertEquals("Test date","Fri Sep 04 12:00:00 ICT 2015",a.getDate().toString());
		Appointment b = new AppDaily("Baitoey", "4/09/2558 12:00");
		assertEquals("Test date","Fri Sep 04 12:00:00 ICT 2015",b.getDate().toString());
		Appointment c = new AppWeekly("Baitoey", "4/09/2558 12:00");
		assertEquals("Test date","Fri Sep 04 12:00:00 ICT 2015",c.getDate().toString());
		Appointment d = new AppMonthly("Baitoey", "4/09/2558 12:00");
		assertEquals("Test date","Fri Sep 04 12:00:00 ICT 2015",d.getDate().toString());
	}

	@Test (expected=ParseException.class)
	public void testCreateAppointmentThrowsException() throws ParseException {
		new AppOnetime("Baitoey", "4:09:2015");
		new AppDaily("Baitoey", "4:09:2015");
		new AppWeekly("Baitoey", "4:09:2015");
		new AppMonthly("Baitoey", "4:09:2015");
		 	 
	}
	
	@Test 
	public void testGetNumAppointment() throws ParseException  {
		AppointmentBook ab = new AppointmentBook();
		ab.add(new AppOnetime("Bozz", "4/09/2558 12:00"));
		assertEquals("Test count number","1",ab.getNumAppointments());
		ab.add(new AppDaily("Bozz", "4/09/2558 12:00"));
		assertEquals("Test count number","2",ab.getNumAppointments());
		ab.add(new AppWeekly("Bozz", "4/09/2558 12:00"));
		assertEquals("Test count number","3",ab.getNumAppointments());
		ab.add(new AppMonthly("Bozz", "4/09/2558 12:00"));
		assertEquals("Test count number","4",ab.getNumAppointments());
	}
	
	@Test 
	public void testAddAppointment() throws ParseException  {
		AppointmentBook ab = new AppointmentBook();
		ab.add(new AppOnetime("Bozz", "4/09/2558 12:00"));
		ab.add(new AppDaily("Bozz", "4/09/2558 12:00"));
		ab.add(new AppWeekly("Bozz", "4/09/2558 12:00"));
		ab.add(new AppMonthly("Bozz", "4/09/2558 12:00"));
	}
	
	@Test 
	public void testAddTwoParameterAppointment() throws ParseException  {
		AppointmentBook ab = new AppointmentBook();
		ab.add("4/09/2558 12:00","baitoey");
	}
	
	@Test 
	public void testAddAllAppointment() throws ParseException  {
		ArrayList<Appointment> list = new ArrayList<Appointment>();
		list.add(new AppOnetime("Bozz", "4/09/2558 12:00") );
		list.add(new AppDaily("Bozz", "4/09/2558 12:00") );
		list.add(new AppWeekly("Bozz", "4/09/2558 12:00") );
		list.add(new AppMonthly("Bozz", "4/09/2558 12:00") );
		AppointmentBook ab = new AppointmentBook();
		ab.addAll(list);
	}
	
	@Test 
	public void testGetDataAppointment() throws ParseException  {
		AppointmentBook ab = new AppointmentBook();
		ab.add(new AppDaily("b", "4/09/2558 12:00") );
		
		assertEquals("Test get","Date :: Fri Sep 04 12:00:00 ICT 2015   Description :: b",ab.getAppointment(0));
	}
	
	@Test 
	public void testCheckDateInListOnetime() throws ParseException  {
		AppointmentBook ab = new AppointmentBook();
		Appointment a = new AppOnetime("Baitoey", "4/09/2558 12:00");
		ab.add(a);
		Appointment b = new AppOnetime("Baitoey", "4/09/2558 12:00");
		assertEquals("Test",false,ab.checkDateInList(b));
	}
	
	@Test 
	public void testCheckDateInListDaily() throws ParseException  {
		AppointmentBook ab = new AppointmentBook();
		Appointment a = new AppDaily("Baitoey", "4/09/2558 12:00");
		ab.add(a);
		Appointment b = new AppDaily("Baitoey", "4/09/2558 12:00");
		assertEquals("Test",false,ab.checkDateInList(b));
	}
	
	@Test 
	public void testCheckDateInListWeekly() throws ParseException  {
		AppointmentBook ab = new AppointmentBook();
		Appointment a = new AppWeekly("Baitoey", "4/09/2558 12:00");
		ab.add(a);
		Appointment b = new AppWeekly("Baitoey", "4/09/2558 12:00");
		assertEquals("Test",false,ab.checkDateInList(b));
	}
	
	@Test 
	public void testCheckDateInListMonth() throws ParseException  {
		AppointmentBook ab = new AppointmentBook();
		Appointment a = new AppMonthly("Baitoey", "4/09/2558 12:00");
		ab.add(a);
		Appointment b = new AppMonthly("Baitoey", "4/09/2558 12:00");
		assertEquals("Test",false,ab.checkDateInList(b));
	}
	
	
=======
		Appointment a = new Appointment("Baitoey", "4/09/2558 12:00");
		assertEquals("Test date","Fri Sep 04 12:00:00 ICT 2015",a.getDate());
	}

	@Test (expected=ParseException.class)
	public void testCreateAppointmentThrowsException() throws ParseException {
		new Appointment("Baitoey", "4:09:2015");
		 	 
	}
	
	@Test 
	public void testGetNumAppointment() throws ParseException  {
		AppointmentBook ab = new AppointmentBook();
		ab.add(new Appointment("Bozz", "4/09/2558 12:00"));
		assertEquals("Test count number","1",ab.getNumAppointments());

	}
	
	@Test 
	public void testAddAppointment() throws ParseException  {
		AppointmentBook ab = new AppointmentBook();
		ab.add(new Appointment("Bozz", "4/09/2558 12:00"));
	}
	
	@Test 
	public void testAddTwoParameterAppointment() throws ParseException  {
		AppointmentBook ab = new AppointmentBook();
		ab.add("4/09/2558 12:00","baitoey");
	}
	
	@Test 
	public void testAddAllAppointment() throws ParseException  {
		ArrayList<Appointment> list = new ArrayList<Appointment>();
		list.add(new Appointment("Bozz", "4/09/2558 12:00") );
		AppointmentBook ab = new AppointmentBook();
		ab.addAll(list);
	}
	
	@Test 
	public void testGetDataAppointment() throws ParseException  {
		AppointmentBook ab = new AppointmentBook();
		ab.add(new Appointment("b", "4/09/2558 12:00") );
		assertEquals("Test get","Date :: Fri Sep 04 12:00:00 ICT 2015   Description :: b",ab.getAppointment(0));
	}
>>>>>>> branch 'master' of https://5610404398@bitbucket.org/5610404398/appointment.git
	
=======
package test.java.ku.sci.th.myapp;

import static org.junit.Assert.*;
import java.text.ParseException;
import java.util.ArrayList;

import org.junit.Test;

import main.java.ku.sci.th.myapp.AppDaily;
import main.java.ku.sci.th.myapp.AppMonthly;
import main.java.ku.sci.th.myapp.AppOnetime;
import main.java.ku.sci.th.myapp.AppWeekly;
import main.java.ku.sci.th.myapp.Appointment;
import main.java.ku.sci.th.myapp.AppointmentBook;


public class TestAppointment {
	@Test
	public void testCreateAppointment() throws ParseException {
		Appointment a = new AppOnetime("Baitoey", "4/09/2558 12:00");
		assertEquals("Test date","Fri Sep 04 12:00:00 ICT 2015",a.getDate().toString());
		Appointment b = new AppDaily("Baitoey", "4/09/2558 12:00");
		assertEquals("Test date","Fri Sep 04 12:00:00 ICT 2015",b.getDate().toString());
		Appointment c = new AppWeekly("Baitoey", "4/09/2558 12:00");
		assertEquals("Test date","Fri Sep 04 12:00:00 ICT 2015",c.getDate().toString());
		Appointment d = new AppMonthly("Baitoey", "4/09/2558 12:00");
		assertEquals("Test date","Fri Sep 04 12:00:00 ICT 2015",d.getDate().toString());
	}

	@Test (expected=ParseException.class)
	public void testCreateAppointmentThrowsException1() throws ParseException {
		new AppOnetime("Baitoey", "4:09:2015");
		new AppDaily("Baitoey", "4:09:2015");
		new AppWeekly("Baitoey", "4:09:2015");
		new AppMonthly("Baitoey", "4:09:2015");
		 	 
	}
	
	@Test 
	public void testGetNumAppointment1() throws ParseException  {
		AppointmentBook ab = new AppointmentBook();
		ab.add(new AppOnetime("Bozz", "4/09/2558 12:00"));
		assertEquals("Test count number","1",ab.getNumAppointments());
		ab.add(new AppDaily("Bozz", "4/09/2558 12:00"));
		assertEquals("Test count number","2",ab.getNumAppointments());
		ab.add(new AppWeekly("Bozz", "4/09/2558 12:00"));
		assertEquals("Test count number","3",ab.getNumAppointments());
		ab.add(new AppMonthly("Bozz", "4/09/2558 12:00"));
		assertEquals("Test count number","4",ab.getNumAppointments());
	}
	
	@Test 
	public void testAddAppointment1() throws ParseException  {
		AppointmentBook ab = new AppointmentBook();
		ab.add(new AppOnetime("Bozz", "4/09/2558 12:00"));
		ab.add(new AppDaily("Bozz", "4/09/2558 12:00"));
		ab.add(new AppWeekly("Bozz", "4/09/2558 12:00"));
		ab.add(new AppMonthly("Bozz", "4/09/2558 12:00"));
	}
	
	@Test 
	public void testAddTwoParameterAppointment1() throws ParseException  {
		AppointmentBook ab = new AppointmentBook();
//		ab.add("4/09/2558 12:00","baitoey");
	}
	
	@Test 
	public void testAddAllAppointment1() throws ParseException  {
		ArrayList<Appointment> list = new ArrayList<Appointment>();
		list.add(new AppOnetime("Bozz", "4/09/2558 12:00") );
		list.add(new AppDaily("Bozz", "4/09/2558 12:00") );
		list.add(new AppWeekly("Bozz", "4/09/2558 12:00") );
		list.add(new AppMonthly("Bozz", "4/09/2558 12:00") );
		AppointmentBook ab = new AppointmentBook();
		ab.addAll(list);
	}
	
	@Test 
	public void testGetDataAppointment() throws ParseException  {
		AppointmentBook ab = new AppointmentBook();
		ab.add(new AppDaily("b", "4/09/2558 12:00") );
		
		assertEquals("Test get","Date :: Fri Sep 04 12:00:00 ICT 2015   Description :: b",ab.getAppointment(0));
	}
	
	@Test 
	public void testCheckDateInListOnetime() throws ParseException  {
		AppointmentBook ab = new AppointmentBook();
		Appointment a = new AppOnetime("Baitoey", "4/09/2558 12:00");
		ab.add(a);
		Appointment b = new AppOnetime("Baitoey", "4/09/2558 12:00");
		assertEquals("Test",false,ab.checkDateInList(b));
	}
	
	@Test 
	public void testCheckDateInListDaily() throws ParseException  {
		AppointmentBook ab = new AppointmentBook();
		Appointment a = new AppDaily("Baitoey", "4/09/2558 12:00");
		ab.add(a);
		Appointment b = new AppDaily("Baitoey", "4/09/2558 12:00");
		assertEquals("Test",false,ab.checkDateInList(b));
	}
	
	@Test 
	public void testCheckDateInListWeekly() throws ParseException  {
		AppointmentBook ab = new AppointmentBook();
		Appointment a = new AppWeekly("Baitoey", "4/09/2558 12:00");
		ab.add(a);
		Appointment b = new AppWeekly("Baitoey", "4/09/2558 12:00");
		assertEquals("Test",false,ab.checkDateInList(b));
	}
	
	@Test 
	public void testCheckDateInListMonth() throws ParseException  {
		AppointmentBook ab = new AppointmentBook();
		Appointment a = new AppMonthly("Baitoey", "4/09/2558 12:00");
		ab.add(a);
		Appointment b = new AppMonthly("Baitoey", "4/09/2558 12:00");
		assertEquals("Test",false,ab.checkDateInList(b));
		Appointment a = new Appointment("Baitoey", "4/09/2558 12:00");
		assertEquals("Test date","Fri Sep 04 12:00:00 ICT 2015",a.getDate());
	}

	@Test (expected=ParseException.class)
	public void testCreateAppointmentThrowsException() throws ParseException {
		new Appointment("Baitoey", "4:09:2015");
		 	 
	}
	
	@Test 
	public void testGetNumAppointment() throws ParseException  {
		AppointmentBook ab = new AppointmentBook();
		ab.add(new Appointment("Bozz", "4/09/2558 12:00"));
		assertEquals("Test count number","1",ab.getNumAppointments());

	}
	
	@Test 
	public void testAddAppointment() throws ParseException  {
		AppointmentBook ab = new AppointmentBook();
		ab.add(new Appointment("Bozz", "4/09/2558 12:00"));
	}
	
	@Test 
	public void testAddTwoParameterAppointment() throws ParseException  {
		AppointmentBook ab = new AppointmentBook();
		ab.add("4/09/2558 12:00","baitoey");
	}
	
	@Test 
	public void testAddAllAppointment() throws ParseException  {
		ArrayList<Appointment> list = new ArrayList<Appointment>();
		list.add(new Appointment("Bozz", "4/09/2558 12:00") );
		AppointmentBook ab = new AppointmentBook();
		ab.addAll(list);
	}
	
	@Test 
	public void testGetDataAppointment() throws ParseException  {
		AppointmentBook ab = new AppointmentBook();
		ab.add(new Appointment("b", "4/09/2558 12:00") );
		assertEquals("Test get","Date :: Fri Sep 04 12:00:00 ICT 2015   Description :: b",ab.getAppointment(0));
	}
>>>>>>> refs/tags/lab4
	
	
}
