<<<<<<< HEAD
package ku.sci.th.myapp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.util.Date;

public class Appointment {
	private String description;
	private Date date;
	private int dayofweek;
	private int day;
	private int month;
	private int year;

	public Appointment(String text, String date) throws ParseException {
		DateFormat dateTimeFormat = new SimpleDateFormat("dd/MM/yy HH:mm");
		dateTimeFormat.setLenient(false);
		Date mytime = dateTimeFormat.parse(date);
		this.date = mytime;
		this.dayofweek = mytime.getDay();
		this.day = mytime.getDate();
		this.month = mytime.getMonth();
		this.year = mytime.getYear();
		this.description = text;
	}

	public Date getDate() {
		return date;
	}

	public int getDayOfWeek() {
		return dayofweek;
	}

	public int getDay() {
		return day;
	}

	public int getMonth() {
		return month;
	}

	public int getYear() {
		return year;
	}

	public String getDescription() {
		return description;
	}

	public boolean occursOn(Date dd) {
		if (dd.getYear() == this.year && dd.getMonth() == this.month
				&& dd.getDate() == this.day) {
			return false;
		}

		return true;
	}
=======
package main.java.ku.sci.th.myapp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.util.Date;

public abstract class Appointment implements Appointed {
	private String description;
	private Date date;
	private int dayofweek;
	private int day;
	private int month;
	private int year;

	public Appointment(String text, String date) throws ParseException {
		DateFormat dateTimeFormat = new SimpleDateFormat("dd/MM/yy HH:mm");
		dateTimeFormat.setLenient(false);
		Date mytime = dateTimeFormat.parse(date);
		String[] s = date.split(" ");
		String datestr = s[0];
		String timrstr = s[1];
		String[] s2 = datestr.split("/");
		this.date = mytime;
		this.dayofweek = mytime.getDay();
		this.day = Integer.parseInt(s2[0]);
		this.month = Integer.parseInt(s2[1]);
		this.year = Integer.parseInt(s2[2]);
		this.description = text;
	}

	public Date getDate() {
		return date;
	}

	public int getDayOfWeek() {
		return dayofweek;
	}

	public int getDay() {
		return day;
	}

	public int getMonth() {
		return month;
	}

	public int getYear() {
		return year;
	}

	public String getDescription() {
		return description;
	}

	abstract boolean occursOn(Date dd);
>>>>>>> refs/tags/lab4

	public String toString() {
		return "Date :: " + this.getDate() + "   Description :: " + description;
	}
}

